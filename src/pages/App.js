import React from 'react';
import { useState } from 'react';

import '../cssStyle/App.css';

import AppHeader from '../components/AppHeader';
import PicStyle from '../components/PicStyle';
import narutoPics from '../data/narutoPics';
import AppFooter from '../components/AppFooter';
import BigPic from '../components/BigPic';
import AppSearch from '../components/AppSearch';

function App() {
  const [selectedPic, setSelectedPic] = useState(null);
  const [searchText, setSearchText] = useState('');

  function onOpenClick(OpenPic) {
    setSelectedPic(OpenPic);
  }

  function onCloseClick() {
    setSelectedPic(null);
  }

  const picElements = narutoPics.filter((naruto) => {
    return naruto.title.includes(searchText);
  }).map((naruto, index) => {
    return <PicStyle key = {index} naruto = {naruto} onPicClick = {onOpenClick}/>;
  });

  let NarutoPic = null;
  if(!!selectedPic) {
    NarutoPic = <BigPic naruto = {selectedPic} onBgClick = {onCloseClick}/>
  }

  return (
    <div className="App">
      <AppHeader />     
      <AppSearch value = {searchText} onValueChange = {setSearchText}/>
     
      <div className = "App-grid">              
        {picElements}
        {NarutoPic}
      </div>        
      
      <AppFooter/>
    </div>
  );
}

export default App;
