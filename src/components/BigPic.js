import '../cssStyle/BigPic.css';

function BigPic(props) {
    const {naruto, onBgClick} = props;
    return (
        <div className = "BigPic">
            <div className = "show-big-pic-bg" onClick = {onBgClick}/>
            <div className = "show-big-pic-content">
                <img src = {naruto.thumbnailUrl} alt = "naruto"/>            
            </div>            
        </div>
    );
}

export default BigPic;