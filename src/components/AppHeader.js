import '../cssStyle/AppHeader.css'

function AppHeader() {
    return(
        <header className = "App-header">
            <img className = "App-header-logo" src = "/pic/narutomemesFont.png" alt = "naruto"/>
        </header>
    );
}

export default AppHeader;