import '../cssStyle/PicStyle.css'

function PicStyle(props) {
  const {naruto, onPicClick} = props;
  return (
      <div className = "picstyle">
        <img src = {naruto.thumbnailUrl} onClick = {() => {onPicClick(naruto)}} alt = "naruto"/>
        <h4>{naruto.title}</h4>
      </div>
  );
}

export default PicStyle;