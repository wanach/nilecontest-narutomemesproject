import '../cssStyle/AppFooter.css'

function AppFooter() {
    return(
        <footer className = "App-footer">
            <p>find more memes <a className = "App-link" href = "https://pin.it/KJnadwc">click here!</a></p>
        </footer>
    );
}

export default AppFooter;